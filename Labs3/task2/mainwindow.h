#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QtCore>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    struct item
    {
        QString name;
        int number;
    };

    QList <item> * list;
    QTimer * timer;

public slots:
    void mySlot(void);
};

#endif // MAINWINDOW_H
