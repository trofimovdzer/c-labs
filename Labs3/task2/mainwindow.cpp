#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    list = new QList<item>;
    item p;
    //------------------------------------
    QFile * file = new QFile(":/items.xml");
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox mess;
        mess.setText("File error!");
        mess.exec();
        return;
    }
    QXmlStreamReader xml(file);
    QXmlStreamReader::TokenType token;
    QString name, number;
    while (!xml.atEnd() && !xml.hasError())
    {
        token=xml.readNext();
        if (token == QXmlStreamReader::StartDocument)
            continue;
        if (token == QXmlStreamReader::StartElement)
        {
            if (xml.name() == "list")
                continue;
            if (xml.name() == "event")
            {
                continue;
            }
            if (xml.name() == "name")
            {
                xml.readNext();
                name=xml.text().toString();
            }
            if (xml.name() == "number")
            {
                xml.readNext();
                number=xml.text().toString();
            }

        }
        if (token == QXmlStreamReader::EndElement)
        {
            if (xml.name() == "event")
            {
                p.name = name;
                p.number = number.toInt();
                list->append(p);
            }
        }
    }
    //-----------------------------------------------
    QList <item> ::Iterator it;
    for(it = list->begin(); it != list->end(); ++it)
    {
        ui->listWidget_2->addItem((*it).name);
        ui->listWidget->addItem(QString::number((*it).number));
    }
    //-----------------------------------------------
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(mySlot()));
    timer->start(1000);

}
void MainWindow::mySlot(void)
{
    ui->listWidget->clear();
    QList <item> ::Iterator it;
    for(it = list->begin(); it != list->end(); ++it)
    {
        if ((*it).number == 1)
        {
            QMessageBox::information(this, "finish!", (*it).name +
                                     " finished!");
        }
        if ((*it).number != 0)
        {
            (*it).number--;
        }
        ui->listWidget->addItem(QString::number((*it).number));
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
