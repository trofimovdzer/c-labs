#include <QCoreApplication>
#include "mytcpsocket.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MyTcpSocket socket;
    socket.doConnect();
    return a.exec();
}
