#-------------------------------------------------
#
# Project created by QtCreator 2014-04-10T08:07:56
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = MyTcpSocket
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    mytcpsocket.cpp

HEADERS += \
    mytcpsocket.h
