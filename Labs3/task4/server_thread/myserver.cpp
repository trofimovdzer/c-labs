#include "myserver.h"
#include "mythread.h"

MyServer::MyServer(QObject *parent) :
    QTcpServer(parent)
{
}

void MyServer::incomingConnection(int sd)
{
    qDebug() << "New client";
    MyThread * th = new MyThread(sd, this);
    connect(th, SIGNAL(finished()), th, SLOT(deleteLater()));
    th->start();
}
