#-------------------------------------------------
#
# Project created by QtCreator 2014-04-27T19:38:51
#
#-------------------------------------------------

QT       += core
QT += network

QT       -= gui

TARGET = server_thread
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    mythread.cpp \
    myserver.cpp

HEADERS += \
    mythread.h \
    myserver.h
