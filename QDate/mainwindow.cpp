#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDate>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->progressBar->setValue(0);
    date = QDate::currentDate();
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(mySlot()));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(mySlot2()));
}
void MainWindow::mySlot()
{

    ui->label->setText(date.toString());
}
void MainWindow::mySlot2()
{
    double d = (double)date.dayOfYear() / date.daysInYear() * 100;

    ui->progressBar->setValue(d);
}

MainWindow::~MainWindow()
{
    delete ui;
}

