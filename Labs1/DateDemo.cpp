//Все будет хорошо
#include <iostream>
#include <showmenu.h>
#include <cstdlib>
#include <ctime>
#define DAY (24 * 60 * 60)
#define HOUR (60 * 60)
#define ABS(x) (((x) >= 0) ? (x) : -(x))
using namespace std;
struct day_hour {
	int day;
	int hour;
};
const char * monthes[] = {"January", "February", "Marth", "April", "May", "June", 
						"July", "August", "September", "October", "November", "December"};
const char * weekdays[] = {"Sanday", "Monday", "Thusday", "Wentsday", "Thursday", "Frayday", "Saturday"};

class DateDemo
{
	tm struct_date;
	time_t sek_date;
	void setStruct(void) { struct_date = *(localtime(&sek_date)); }
	void setSek(void) { sek_date = mktime(&struct_date); }
	time_t newDate(time_t sek) { return sek_date + sek; }
	time_t _temp;

	public:
	DateDemo() { sek_date = time(NULL); setStruct(); }
	DateDemo(int year, int month, int day, int hour = 0, int minute = 0, int sec = 0);
	DateDemo(time_t s) { sek_date = s; setStruct(); }
	void printToday(void) { cout << "Date: " << asctime(&struct_date) << endl; }
	void printYesterday(void) { cout << "Date: " << ctime(&(_temp=newDate(-DAY))) << endl; }
	void printTomorrow(void) { cout << "Date: " << ctime(&(_temp=newDate(DAY))) << endl; }
	void printFuture(int n) { cout << "Date: " << ctime(&(_temp=newDate(n * DAY))) << endl; }
	void printPast(int n) { cout << "Date: " << ctime(&(_temp=newDate(-n * DAY))) << endl; }
	void printMonth(void) { cout << monthes[struct_date.tm_mon] << endl; }
	void printWeekday(void) { cout << weekdays[struct_date.tm_wday] << endl; }
	int getYear(void) { return struct_date.tm_year + 1900; }
	friend day_hour calcDifference(DateDemo & d1, DateDemo & d2);
};
day_hour calcDifference(DateDemo & d1, DateDemo & d2)
{
	int result = d1.sek_date - d2.sek_date;
	result = ABS(result);
	day_hour temp;
	temp.day = result / DAY;
	temp.hour = (result % DAY) / HOUR;

	return temp;
}

DateDemo::DateDemo(int year, int month, int day, int hour, int minute, int sec)
{
	struct_date.tm_sec=sec;
	struct_date.tm_min=minute;
	struct_date.tm_hour=hour;
	struct_date.tm_mday=day;
	struct_date.tm_mon=month-1;
	struct_date.tm_year=year-1900;
	setSek();
}
string s_arr[] = {
				"Today",
				"Yesterday",
				"Tommorow",
				"Some days later",
				"Some days before",
				"Current month and weekday",
				"Days and hours to New Year"
				};
int main()
{
	Menu menu(s_arr, 7);
	char ch;
	int y, m, d;
	int n_day;
	cout << "Enter year, month, day" << endl;
	cin >> y >> m >> d;
	DateDemo today(y, m, d);
	DateDemo new_year(today.getYear() + 1, 1, 1);
	day_hour temp  = calcDifference(today, new_year);
	while ((ch = menu.show()) != 'q')
		switch (ch)
		{
			case 'a':
			cout << "Today:" << endl;
			today.printToday();
			break;
			case 'b':
			cout << "Yesterday:" << endl;
			today.printYesterday();
			break;
			case 'c':
			cout << "Tomorrow:" << endl;
			today.printTomorrow();
			break;
			case 'd':
			cout << "Enter number of days:" << endl;
			cin >> n_day;
			cout << n_day << " days later:" << endl;
			today.printFuture(n_day);
			break;
			case 'e':
			cout << "Enter number of days:" << endl;
			cin >> n_day;
			cout << n_day << " days before:" << endl;
			today.printPast(n_day);
			break;
			case 'f':
			cout << "Now is: " << endl;
			today.printMonth();
			today.printWeekday();
			cout << endl;
			break;
			case 'g':
			cout << "To New Year is:" << endl;
			cout << temp.day << " days." << endl
				<< temp.hour << " hours." << endl;
			break;
			default:
			cout << "Bay bay" <<endl;
			exit(0);
		}

	return 0;
}




