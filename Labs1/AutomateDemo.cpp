//Все будет хорошо
#include <iostream>
#include <string>
#include <showmenu.h> //put showmenu.h in /usr/include
#include <cstring>
#include <unistd.h>
#include <cstdlib>
#define CH1 "50"
#define CH2 "20"
#define CH3 "75"
const string for_off[] = {"On."};
const string for_on[] = {"Put money."};
using namespace std;
class Automate
{
	//Data
	int cash;
	long all_money;
	static const string for_choice [3];
	static const int prices [3];
	//help function
	protected:
	void niceTitle(const char * s);
	void beautiTitle(const char * s);
	void cleanScreen(void);
	void putMoney(void);
	void line(void);
	//main function
	void on(void);
	void choice(void);
	void work(void);
	public:
	void off(void);
	//constructors
	Automate(int m = 0) : all_money(m) { cash = 0; }
};
const string Automate::for_choice[3] = { "Just black coffee. " CH1  " rubels.", "Lemon tee. " CH2 " rubels",
											 "Capuchino. " CH3 "rubels." };
const int Automate::prices[3] = { atoi(CH1), atoi(CH2), atoi(CH3) };
//-----------------------------------------HELP FUNCTION------------------------------------
void Automate::putMoney(void)
{
	cout << "Put your money, please:" << endl;
	cin >> cash;
	if (cash <= 0)
	{ cout << "Cash should be > 0. " << endl;
		putMoney();
	}
}
void Automate::cleanScreen(void)
{
	int n = 40, i;
	for (i = 0; i < n; ++i)
		cout << endl;
}
void Automate::line(void)
{
	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl
		<< "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
}
void Automate::niceTitle(const char * s)
{
	cleanScreen();
	line();
	cout << s << endl;
}
void Automate::beautiTitle(const char * s)
{
	cleanScreen();
	line();
	cout << s << endl;
	line();
}

//---------------------------------------------MAIN FUNCTION---------------------------------------
void Automate::off(void)
{
	Menu off_menu(for_off, 1);
	char ch;
	niceTitle("I am sleeping");
	while ((ch = off_menu.show()) != 'q')
	{
		switch (ch)
		{
			case 'a':
				on();
				break;
		}
		niceTitle("I am sleeping");
	}
}
void Automate::on(void)
{
	Menu on_menu(for_on, 1);
	char ch;
	niceTitle("I am waiting for money.");
	while ((ch = on_menu.show()) != 'q')
	{
		switch (ch)
		{
			case 'a':
				putMoney();
				choice();
				break;
		}
		niceTitle("I am waiting for money.");
	}
}
void Automate::choice(void)
{
	Menu choice_menu(for_choice, 3);
	char ch;
	int i;
	long rest;
	niceTitle("I am waiting for choice.");
	while ((ch = choice_menu.show()) != 'q')
	{
		i = -1;
		switch (ch)
		{
			case 'a':
				i = 0;
				break;
			case 'b':
				i = 1;
				break;
			case 'c':
				i = 2;
				break;
			default:
				niceTitle("I am waiting for choice.");
				continue;
		}
		if (prices[i] > cash)
		{
			niceTitle("Price of this drink is bigger then your money.\n"
						"Try choice other drink");
			continue;
		}
		rest = cash - prices[i];
		all_money += prices[i];
		work();
		beautiTitle("Here is your rest and your dish.\n"
					"Have a good appetite");
		cout << "Dish: " << for_choice[i] << endl;
		cout << "Rest: " << rest << endl;
		goto wait;
	}
	beautiTitle("Here is your money: ");
	cout << cash << endl;
wait:
	int count = 9;
	cout << "Wait:" << endl;
	while (count >= 0)
	{
		cerr << count << " sekend.\r";
		count--;
		sleep(1);
	}
	cout << endl;
}
void Automate::work(void)
{
	beautiTitle("I am working");
	cout << endl;
	cerr << "               (100%)\r";
	for (int i = 0; i < 15; i++)
	{
		cerr << "#";
		sleep(1);
	}
	cout << endl;
}
int main()
{
	Automate bosh;
	bosh.off();

	return 0;
}
