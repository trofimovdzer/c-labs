//Все будет хорошо
#include <iostream>
#include <string>
using namespace std;
struct money {
	int big;
	int small;
	long big_small;
};
class Currency
{
	static double curs_dollar;
	static double  curs_euro;
	string name;
	money rubls;
	money dollars;
	money euros;
	long toIn(money & m);
	void toDes(money & m);

	public:
	Currency(const string & _name = "None", int b = 0, int c = 0);
	void refresh(void);
	Currency & operator+=(long m);
	Currency & operator-=(long m);
	bool operator==(Currency & c) { return (rubls.big_small == c.rubls.big_small ? true : false); }
	bool operator!=(Currency & c) { return (! ((*this) == c)); }
	//friends
	friend ostream & operator<<(ostream & os, Currency & c);
	friend istream & operator>>(istream & is, Currency & c);
	friend void setCurs(void);
};
double Currency::curs_dollar = 0;
double Currency::curs_euro = 0;
long Currency::toIn(money & m)
{
	m.big_small = m.big * 100 + m.small;
	return m.big_small;
}
void Currency::toDes(money & m)
{
	m.big = m.big_small / 100;
	m.small = m.big_small % 100;
}
void Currency::refresh(void)
{
	if (curs_dollar  == 0)
		dollars.big_small = 0;
	else
		dollars.big_small = (double)rubls.big_small  / curs_dollar;
	toDes(dollars);
	if (curs_euro == 0)
		euros.big_small = 0;
	else
		euros.big_small = (double)rubls.big_small / curs_euro;
	toDes(euros);
}
Currency::Currency(const string & _name, int b, int c)
{
	name = _name;
	rubls.big = b;
	rubls.small = c;
	toIn(rubls);
	refresh();
}
Currency & Currency::operator+=(long m)
{
	rubls.big_small += m;
	toDes(rubls);
	refresh();
	return *this;
}
Currency & Currency::operator-=(long m)
{
	rubls.big_small -= m;
	toDes(rubls);
	refresh();
	return *this;
}
ostream & operator<<(ostream & os, Currency & c)
{
	os << "Name: " << endl
		<< c.name << endl
		<< "Rub: " << c.rubls.big << " cop: " << c.rubls.small << endl
		<< "Dol: " << c.dollars.big << " cent: " << c.dollars.small << endl
		<< "Euro: " << c.euros.big << " cent: " << c.euros.small;
	
	return os;
}
istream & operator>>(istream & is, Currency & c)
{
	string temp;
	int rub, cop;
	cout << "Enter name please: " << endl;
	is >> temp;
	cout << "Enter rubls, cop: " << endl;
	is >> rub >> cop;
	while (is.get() != '\n')
		;
	c = Currency(temp, rub, cop);
	return is;
}
void setCurs(void)
{
	cout << "Enter new curs for dollar (e.x. 26.44):" << endl;
	cin >> Currency::curs_dollar;
	cout << "Enter new curs for euro (e.x. 45.34):" << endl;
	cin >> Currency::curs_euro;
}
const int N = 3;
int main()
{
	Currency arr[N];
	int i;
	for (i = 0; i < N; ++i)
		cin >> arr[i];
	cout << "Change and show: " << endl;
	arr[0] += 100;
	arr[1] -= 100;
	for (i = 0; i < N; ++i)
		cout << arr[i] << endl;
	cout << "Set curses: " << endl;
	setCurs();
	for (i = 0; i < N; ++i)
		arr[i].refresh();
	cout << "Show: " << endl;
	for (i = 0; i < N; ++i)
		cout << arr[i] << endl;
	cout << "arr[0] != arr[2] ?" << endl;
	if (arr[0] != arr[2])
		cout << "True." << endl;
	else
		cout << "False." << endl;
	
	return 0;
}

	 

