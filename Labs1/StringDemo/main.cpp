//Все будет хорошо
#include <iostream>
#include "MyStr.h"
using namespace std;
int main()
{
	cout << "============================================" << endl;
	MyStr s1 = "Hello, ";
	MyStr s2 = "world!";
	MyStr s3;
	s3 = s1 + s2;
	cout << s3 << endl;
	cout << "============================================" << endl;
	s3 = s1 + s2 + " Again.";
	cout << s3 << endl;
	cout << "============================================" << endl;
	int i;
	for (i = 0; i < s3.length(); ++i)
		cout << s3[i];
	cout << endl;
	cout << "============================================" << endl;
	cout << "Enter the string: " << endl;
	cin >> s1;
	cout << s1 << endl;
	cout << "============================================" << endl;
	cout << s3.getStr() << endl;
	cout << "============================================" << endl;
	s3 += " Again.";
	cout << s3 << endl;
	cout << "============================================" << endl;
	s1 = s3;
	if (!(s1 != s3))
		cout << "s1 == s3" << endl;
	cout << "============================================" << endl;
	s3 = s3 ^ 3;
	cout << s3;
	cout << "============================================" << endl;
	cout << (s3.searchChar('e'))[0] << (s3.searchChar('n'))[0] << (s3.searchChar('d'))[0] << endl;

	return 0;
}


