//Все будет хорошо
#include <cctype>
#include "String.h"
#include <cstring>
using namespace std;
//static class functions, constructors, deconstuctors
int String::m_count = 0;
int String::howMany()
{
	return m_count;
}
String::String()
{
	m_str = new char[1];
	m_str[0] = '\0';
	m_len = 0;
	m_count++;
}
String::String(const String & s)
{
	m_len = strlen(s.m_str);
	m_str = new char[m_len + 1];
	strcpy(m_str, s.m_str);
	m_count++;
}
String::String(const char * str)
{
	m_len = strlen(str);
	m_str = new char[m_len + 1];
	strcpy(m_str, str);
	m_count++;
}
String::~String()
{
	delete [] m_str;
	m_str = NULL;
	m_count--;
}
//member functions
int String::length()
{
	return m_len;
}
bool String::operator>(String & s)
{
	return (strcmp(m_str, s.m_str) > 0);
}
bool String::operator==(String & s)
{
	return (strcmp(m_str, s.m_str) == 0);
}
String & String::operator=(const String & s)
{
	delete [] m_str;
	m_len = s.m_len;
	m_str = new char[m_len + 1];
	strcpy(m_str, s.m_str);

	return *this;
}
String & String::operator=(const char * str)
{
	delete [] m_str;
	m_len = strlen(str);
	m_str = new char[m_len + 1];
	strcpy(m_str, str);

	return *this;
}
char & String::operator[](int i)
{
	return m_str[i];
}
//friend functions
bool operator<(String & s1, String & s2)
{
	return s2 > s1;
}
istream & operator>>(istream & os, String & s)
{
	char buf[NBUF];
	os.get(buf, NBUF);
	if (strlen(buf))
		s = buf;
	os.clear();
	while (os.get() != '\n')
		;

	return os;
}
ostream & operator<<(ostream & os, String & s)
{
	os << s.m_str;

	return os;
}
//newwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
String String::operator+(const String & s)
{
		int len = m_len + s.m_len;
		char str[len + 1];
		str[0] = '\0';
		strcat(str, m_str);
		strcat(str, s.m_str);

		return String(str);
}
String & String::toLow()
{
	int i;
	for (i = 0; i < m_len; ++i)
		m_str[i] = tolower(m_str[i]);
	
	return *this;
}
String & String::toUp()
{
	int i;
	for (i = 0; i < m_len; ++i)
		m_str[i] = toupper(m_str[i]);
	
	return *this;
}
int String::has(char ch)
{
	int tot = 0, i;
	for (i = 0; i < m_len; ++i)
		if (m_str[i] == ch)
			tot++;
	
	return tot;
}
String operator+(const char * cs, const String & s)
{
	String str = cs;
	return (str + s);
}

	

	

		

