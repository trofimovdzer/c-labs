//Все будет хорошо
#ifndef HFILE_H_
#define HFILE_H_
#include <iostream>
const int NBUF = 1024;
const int N =5;
class String
{
	private:
	char * m_str;
	int m_len;
	static int m_count;

	public:
	//constructors, destructors
	String();
	String(const String &);
	String(const char *);
	virtual ~String();
	static int howMany();
	//member functions
	int length();
	bool operator>(String &);
	bool operator==(String &);
	String & operator=(const String &);
	String & operator=(const char *);
	char & operator[](int);
	//friends
	friend bool operator<(String &, String &);
	friend std::istream & operator>>(std::istream &, String &);
	friend std::ostream & operator<<(std::ostream &, String &);
	//newwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	virtual String operator+(const String &);
	friend String operator+(const char *, const String &);
	String & toLow();
	String & toUp();
	int has(char);
	const char * getStr()  const { return m_str; }
};
#endif
