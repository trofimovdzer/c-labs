//Все будет хорошо
#ifndef MYSTR_H_
#define MYSTR_H_
#include "String.h"
class MyStr : public String
{
	public:
	//standart
	MyStr() : String() {}
	MyStr(const MyStr & m) : String(m) {}
	MyStr(const char * s) :String(s) {}
	MyStr(const String & s) : String(s) {}
	~MyStr() {};
	MyStr & operator=(const MyStr & m) { String::operator=(m); }
	MyStr & operator=(const char * s) { String::operator=(s); }
	//no standart
	MyStr & operator+=(const MyStr & m) { *this = (*this) + m; return *this; }
	bool operator==(const MyStr & m) const;
	bool operator!=(const MyStr & m);
	MyStr operator^(int n);
	const char * searchChar(char ch);
	const char * searchStr(const char * s);
};
#endif
