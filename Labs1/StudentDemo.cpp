//Все будет хорошо
#include <iostream>
#include <string>
using namespace std;
class Student
{
	private:
	string name;
	int num;
	int * scores;

	public:
	Student(const string & _name = "None", int _num = 5);
	Student(const Student & s);
	Student & operator=(const Student & s);
	virtual ~Student();
	void set(void);
	double average(void);
	void print(void);
	const string & getName() { return name; }
};
class Group 
{
	private:
	enum {NUM = 25};
	Student arr[NUM]; 
	int count;

	public:
	Group(): count(0) {}
	Group(const Group & g);
	Group & operator=(const Group & g);
	~Group() {}
	void add(void);
	void del(void);
	double average(void);
	void print(void);
};
void clean(void)
{
	while (cin.get() != '\n')
		;
}
//------------------------------------CLASS STUDENT----------------------------------
Student::Student(const string & _name, int _num) : name(_name), num(_num)
{
	if (_num <= 0)
	{
		scores = NULL;
		return;
	}
	scores = new int[_num];
}
Student::Student(const Student & s)
{
	name = s.name;
	num = s.num;
	scores = new int [num];
	int i;
	for (i = 0; i < num; ++i)
		scores[i] = s.scores[i];
}
Student & Student::operator=(const Student & s)
{
	if (this == &s)
		return *this;
	name = s.name;
	num = s.num;
	delete [] scores;
	scores = new int [num];
	int i;
	for (i = 0; i < num; ++i)
		scores[i] = s.scores[i];
	
	return *this;
}
Student::~Student()
{
	delete [] scores;
}
void Student::set(void)
{
	int i;
	for (i = 0; i < num; ++i)
	{
		cout << "Enter " << "#" << i << " score:" << endl;
		cin >> scores[i];
	}
}
double Student::average(void)
{
	int i;
	double aver = 0;
	for (i = 0; i < num; ++i)
		aver += scores[i];
	aver = aver / num;
	return aver;
}
void Student::print(void)
{
	int i;
	cout << "name: " << name << endl;
	cout << "scores: " ;
	for (i = 0; i < num; i++)
		cout << scores[i] << " " ;
	cout << endl;
	cout << "average: " << average() << endl;
}
//--------------------------------end class student-------------------------------------
//-------------------------------class group--------------------------------------------
Group::Group(const Group & g)
{
	int i;
	count = g.count;
	for (i = 0; i < g.count; i++)
		arr[i] = g.arr[i];
}
Group & Group::operator=(const Group & g)
{
	int i;
	count = g.count;
	for (i = 0; i < g.count; i++)
		arr[i] = g.arr[i];
}
void Group::add(void)
{
	string temp;
	cout << "Enter student name, please:" << endl;
	getline(cin, temp);
	arr[count] = Student(temp);
	arr[count].set();
	count++;
	cout << "----------------------------------------" << endl;
	clean();
}
void Group::del(void)
{
	string temp;
	int fl = -1;
	cout << "Enter student name, please:" << endl;
	getline(cin, temp);
	int i;
	for (i = 0; i < count; ++i)
		if (arr[i].getName() == temp)
		{
			fl = i;
			break;
		}
	if (fl == -1)
	{
		cerr << "Can't deleted. Operation deletion termitated" << endl;
		return;
	}
	arr[fl] = arr[count -1];
	count--;
	cout << "Operation continued successfully" << endl;
}	
double Group::average(void)
{
	double aver = 0;
	int i;
	if (count == 0)
	{
		cerr << "Error: function average. count == 0." << endl;
		return -1;
	}
	for (i = 0; i < count; ++i)
		aver += arr[i].average();
	aver = aver / count;
	return aver;
}
void Group::print(void)
{
	int i;
	for (i = 0; i < count; i++)
	{
		arr[i].print();
		cout << "-------------------------------" << endl;
	}
}
int main()
{
	int n_stud, n_score;
	cout << "Enter number of student [0, 24]:" << endl;
	cin >> n_stud;
	clean();
	Group group;
	int i;
	for (i = 0; i < n_stud; i++)
		group.add();
	cout << "Now you can delete any person." << endl;
	group.del();
	cout << "-------------------------------" << endl;
	cout << "Here you can see full list." << endl;
	group.print();
	cout << "-------------------------------" << endl;
	cout << "Here you can see average score of all group:" << endl;
	cout << group.average() << endl;

	return 0;
}
	

