#ifndef SHOWMENU_H_
#define SHOWMENU_H_
#include <string>
using std::string;
class Menu
{
	private:
	enum {N = 10};
	string m_arr[N];
	string m_quit;
	int m_count;
	char m_enter(void) const;

	public:
	Menu(const string * p, int count = 0); 
	char show(void) const;
};
#endif
