#include "showmenu.h"
#include <cctype>
#include <iostream>
using namespace std;
static const char * abc = "abcdefghij";
static void clean(void)
{
	while (cin.get() != '\n')
		;
}
Menu::Menu(const string * p, int n)
{
	int i;
	m_count = n;
	for (i = 0; i < n; i++)
		m_arr[i] = p[i];
	m_quit = "q) to exit";
}
char Menu::show(void) const
{
	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl
		<< "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
	int i;
	for (i = 0; i < m_count; i++)
		cout << abc[i] << ")  " << m_arr[i] << endl;
	cout << m_quit << endl;
	char ch;
	ch = m_enter();

	return ch;
}
char Menu::m_enter(void) const
{
	char ch;
	int i;
	bool flag = true;
	do
	{
		cout << "Enter the corresponding letter:" << endl;
		cin >> ch;
		clean();
		ch = tolower(ch);
		for (i = 0; i < m_count; i++)
			if (ch == abc[i])
				flag = false;
		if (ch == 'q')
			flag = false;
	}
	while (flag);

	return ch;
}







	
	


