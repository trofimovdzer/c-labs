#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    readData();
    setXY();
    ui->setupUi(this);
    wal = QImage(":/wal.jpg").scaled(30, 30);
    grass = QImage(":/grass.jpg").scaled(30, 30);
    men = QImage(":/men.jpg").scaled(30, 30);
    QTimer * timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(500);

    resize(30 * w, 30 * h);
}
void Dialog::readData()
{
    QFile file(":/data.txt");
    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream stream(&file);
        stream >> w >> h;
        stream >> fin_x >> fin_y;
        arr.resize(h);
        stream.readLine();
        for(int i = 0; i < h; i++)
        {
            arr[i]=stream.readLine();
        }
    }
    else
        exit(1);
}
void Dialog::setXY()
{
    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++)
            if (arr[i][j] == 'x')
            {
                cur_x = i;
                cur_y = j;
                return;
            }
    QMessageBox::information(this, "Error!", "Uncorrect data");
    exit(1);
}

void Dialog::paintEvent(QPaintEvent *event)
{
    move_people();
    QPainter painter (this);
    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++)
            if (arr[i][j] == '#')
                painter.drawImage(j * 30, i * 30, wal);
            else if (arr[i][j] == ' ' || arr[i][j] == '.' ||
                     arr[i][j] == 's')
                painter.drawImage(j * 30, i * 30, grass);
            else
                painter.drawImage(j * 30, i * 30, men);


}
void Dialog::move_people(void)
{
    if (cur_x == fin_x && cur_y == fin_y)
    {
        QMessageBox::information(this, "FINISH", "FINISH");
        exit(0);
    }
    if (arr[cur_x][cur_y + 1] == ' ')
    {
        arr[cur_x][cur_y] = '.';
        cur_y++;
        arr[cur_x][cur_y] = 'x';
        return;
    }
    if (arr[cur_x - 1][cur_y] == ' ')
    {
        arr[cur_x][cur_y] = '.';
        cur_x--;
        arr[cur_x][cur_y] = 'x';
        return;
    }
    if (arr[cur_x][cur_y - 1] == ' ')
    {
        arr[cur_x][cur_y] = '.';
        cur_y--;
        arr[cur_x][cur_y] = 'x';
        return;
    }
    if (arr[cur_x + 1][cur_y] == ' ')
    {
        arr[cur_x][cur_y] = '.';
        cur_x++;
        arr[cur_x][cur_y] = 'x';
        return;
    }
    //--------------------------------
    if (arr[cur_x][cur_y + 1] == '.')
    {
        arr[cur_x][cur_y] = 's';
        cur_y++;
        arr[cur_x][cur_y] = 'x';
        return;
    }
    if (arr[cur_x - 1][cur_y] == '.')
    {
        arr[cur_x][cur_y] = 's';
        cur_x--;
        arr[cur_x][cur_y] = 'x';
        return;
    }
    if (arr[cur_x][cur_y - 1] == '.')
    {
        arr[cur_x][cur_y] = 's';
        cur_y--;
        arr[cur_x][cur_y] = 'x';
        return;
    }
    if (arr[cur_x + 1][cur_y] == '.')
    {
        arr[cur_x][cur_y] = 's';
        cur_x++;
        arr[cur_x][cur_y] = 'x';
        return;
    }


}


Dialog::~Dialog()
{
    delete ui;
}
