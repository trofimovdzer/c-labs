#-------------------------------------------------
#
# Project created by QtCreator 2014-05-04T14:56:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = task3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    widget.cpp

HEADERS  += mainwindow.h \
    widget.h \
    hfile.h

FORMS    += mainwindow.ui

RESOURCES += \
    res.qrc
