#ifndef HFILE_H
#define HFILE_H
#include <time.h>
#include <stdlib.h>
#include <typeinfo>
const int N = 20;
const int M = 20;
class Point
{
public:
    virtual int getLive(){}
    virtual void up(){}
};
class Stone : public Point
{

};
class Tree : public Point
{

};
class Rabbit : public Point
{
    int live;
public:
    Rabbit() : live(5) {}
    int getLive() { return live; }
    void up() { live--; }
};
class Wolf : public Point
{
    int live;

public:
    Wolf() : live(5) {}
    int getLive() { return live; }
    void up() { live--; }
};

#endif // HFILE_H
