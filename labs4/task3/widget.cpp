#include "widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
    wolf = QImage(":/wolf.jpg").scaled(100, 100);
    wolf_l = new QLabel(this);
    wolf_l->setPixmap(QPixmap::fromImage(wolf));
    rabbit = QImage(":/rabbit.jpg").scaled(100, 150);
    rabbit_l = new QLabel(this);
    rabbit_l->setPixmap(QPixmap::fromImage(rabbit));

    sl_wolf = new QSlider(Qt::Horizontal);
    sl_wolf->setMinimum(1);
    sl_wolf->setMaximum(10);
    sl_rabbit = new QSlider(Qt::Horizontal);
    sl_rabbit->setMinimum(10);
    sl_rabbit->setMaximum(100);

    but= new QPushButton("Set");
    connect(but, SIGNAL(clicked()), this, SLOT(end()));

    lay = new QVBoxLayout;
    lay->addWidget(new QLabel("Set wolfs"));
    lay->addWidget(wolf_l);
    lay->addWidget(sl_wolf);
    lay->addWidget(new QLabel("Set rabbits"));
    lay->addWidget(rabbit_l);
    lay->addWidget(sl_rabbit);
    lay->addWidget(but);


    setLayout(lay);

    move(600, 100);
}
