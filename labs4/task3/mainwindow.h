#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtWidgets>
#include "hfile.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void setNumbers(int _nw, int _nr)
    {
        nw = _nw;
        nr = _nr;
     }
    void start(void)
    {
        iniMatrix();
        show();
        timer->start(1000);
    }

protected:
    void paintEvent(QPaintEvent *);
private:
    //image
    QImage i_wolf;
    QImage i_rabbit;
    QImage i_stone;
    QImage i_tree;
    //types
    enum {N=20, M=20};
    enum {NSTONES=10};
    //functions
    void iniMatrix();
    void stepRabit();
    void stepWolf();
    bool valid(int i, int j)
    {
        if (i <0 || i >=N ||
                j <0 || j >=M)
            return false;
        return true;
    }

    //data
    Ui::MainWindow *ui;
    int nw, nr;
    Point * matrix[N][M];
    QTimer * timer;

    //flag
    bool fl;
};

#endif // MAINWINDOW_H
