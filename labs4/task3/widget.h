#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtCore>
#include <QtWidgets>

class Widget : public QWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);

signals:
    void send(int nw, int nr);
    void next();
public slots:
    void end()
    {
        emit send(sl_wolf->value(), sl_rabbit->value());
        emit next();
        close();
    }

private:
    QImage wolf;
    QLabel * wolf_l;
    QImage rabbit;
    QLabel * rabbit_l;
    QVBoxLayout * lay;
    QSlider * sl_wolf;
    QSlider * sl_rabbit;
    QPushButton * but;

};

#endif // WIDGET_H
