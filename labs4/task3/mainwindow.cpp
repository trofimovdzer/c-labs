#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <stdlib.h>
#include <time.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    fl = false;
    i_wolf = QImage(":/wolf.jpg").scaled(30, 30);
    i_rabbit = QImage(":/rabbit.jpg").scaled(30, 30);
    i_stone = QImage(":/stone.jpg").scaled(30, 30);
    i_tree = QImage(":/tree.jpg").scaled(30, 30);
    timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    move(10, 10);
    resize(N * 30, M * 30 +20);
    srand(time(NULL));
}
void MainWindow::iniMatrix()
{
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            matrix[i][j] = new Tree;
    int count = NSTONES;
    srand(time(NULL));
    while (count)
    {
        int i = rand() % N;
        int j = rand() % M;
        if (typeid(*matrix[i][j]) != typeid(Stone))
        {
            matrix[i][j] = new Stone;
            count--;
        }
    }
    count = nw;
    while (count)
    {
        int i = rand() % N;
        int j = rand() % M;
        if (typeid(*matrix[i][j]) != typeid(Wolf) && typeid(*matrix[i][j]) != typeid(Stone))
        {
            matrix[i][j] = new Wolf;
            count--;
        }
    }
    count = nr;
    while (count)
    {
        int i = rand() % N;
        int j = rand() % M;
        if (typeid(*matrix[i][j]) != typeid(Wolf) && typeid(*matrix[i][j]) != typeid(Stone)
                && typeid(*matrix[i][j]) != typeid(Rabbit))
        {
            matrix[i][j] = new Rabbit;
            count--;
        }
    }
}
void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    if (fl)
    {
        stepRabit();
        stepWolf();
    }
    else
        fl = true;
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            if (typeid(*matrix[i][j]) == typeid(Tree))
                painter.drawImage(i * 30, j * 30, i_tree);
            else if (typeid(*matrix[i][j]) == typeid(Stone))
                painter.drawImage(i * 30, j * 30, i_stone);
            else if (typeid(*matrix[i][j]) == typeid(Wolf))
                painter.drawImage(i * 30, j * 30, i_wolf);
            else
                painter.drawImage(i * 30, j * 30, i_rabbit);
}
void MainWindow::stepRabit()
{
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            if (typeid(*matrix[i][j]) == typeid(Rabbit))
            {
                //sex
                int x = i + rand() % 3 - 1;
                int y = j + rand() % 3 - 1;
                if (valid(x, y) &&
                        typeid(*matrix[x][y]) == typeid(Tree))
                    matrix[x][y] = new Rabbit;
                //++
                matrix[i][j]->up();
                //dai
                if (matrix[i][j]->getLive() <= 0)
                {
                    delete matrix[i][j];
                    matrix[i][j] = new Tree;
                    continue;
                }
                //move
                x = i + rand() % 3 - 1;
                y = j + rand() % 3 - 1;
                if (valid(x, y) &&
                        typeid(*matrix[x][y]) == typeid(Tree))
                {
                    Point * p = matrix[x][y];
                    matrix[x][y] = matrix[i][j];
                    matrix[i][j] = p;
                }

            }
}
void MainWindow::stepWolf()
{
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            if (typeid(*matrix[i][j]) == typeid(Wolf))
            {
                bool flag = false;
                //food
                for (int x = i - 1; x <= i + 1 && !flag; x++)
                    for (int y = j - 1; y <= j + 1 && !flag; y++)
                        if ( valid(x, y) &&
                             typeid(*matrix[x][y]) == typeid(Rabbit))
                        {
                            flag = true;
                            delete matrix[x][y];
                            matrix[x][y] = new Tree;
                        }
                //++
                matrix[i][j]->up();
                //dai
                if (!flag || matrix[i][j]->getLive() <= 0)
                {
                    delete matrix[i][j];
                    matrix[i][j] = new Tree;
                    continue;
                }
                //sex
                int x = i + rand() % 3 - 1;
                int y = j + rand() % 3 - 1;
                if (valid(x, y) &&
                        typeid(*matrix[x][y]) == typeid(Tree))
                    matrix[x][y] = new Wolf;
                //move
                x = i + rand() % 3 - 1;
                y = j + rand() % 3 - 1;
                if (valid(x, y) &&
                        typeid(*matrix[x][y]) == typeid(Tree))
                {
                    Point * p = matrix[x][y];
                    matrix[x][y] = matrix[i][j];
                    matrix[i][j] = p;
                }



            }

}

MainWindow::~MainWindow()
{
    delete ui;
}
