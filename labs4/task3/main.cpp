#include "mainwindow.h"
#include <QApplication>
#include "widget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    Widget first;
    first.show();
    QObject::connect(&first, SIGNAL(send(int,int)), &w, SLOT(setNumbers(int,int)));
    QObject::connect(&first, SIGNAL(next()), &w, SLOT(start()));

    return a.exec();
}
