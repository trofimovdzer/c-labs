#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    label = new QLabel(this);
    label->setText("<b>Hello</b>");
}

Dialog::~Dialog()
{
    delete ui;
}
