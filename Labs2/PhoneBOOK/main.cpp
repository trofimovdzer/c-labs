#include <iostream>
#include <fstream>
#include <showmenu.h>
#include <string>
#include <cstdlib>
#include <map>
#include <algorithm>

#define FILE_NAME "data.txt"

using namespace std;

typedef pair<int, string> PairM;
typedef map<int,string> BookMap;
void show(const PairM & p)
{
    cout << "Phone: " << p.first << "    Name: " << p.second << endl;
}

const int N = 5;
const string strs[N] =
{
    "Add",
    "Show",
    "Search by phone number",
    "Search by name",
    "Delete"
};

void clean(void);

void add(BookMap &b);
void refreshData(BookMap & book);
void lookForK(BookMap &book);
void lookForN(BookMap & book);
void deleteStr(BookMap & book);


int main()
{
    BookMap book;
    BookMap::iterator it;
    ifstream in;
    string temp_str;
    int temp_int;
    in.open(FILE_NAME);
    if (! in)
    {
        cerr << "Error!\n" << endl;
        return 1;
    }
    while (in >> temp_str)
    {
        temp_int = atoi(temp_str.c_str());
        getline(in, temp_str);
        book.insert(PairM(temp_int, temp_str));
    }
    in.close();

    Menu menu(strs, N);
    char ch;
    while ((ch = menu.show()) != 'q')
    {
        cout << "Size: " << book.size() << endl;
        switch (ch) {
        case 'a':
            add(book);
            refreshData(book);
            break;
        case 'b':
             for_each(book.begin(), book.end(), show);
             break;
        case 'c':
            lookForK(book);
            break;
        case 'd':
            lookForN(book);
            break;
        case 'e':
            deleteStr(book);
            refreshData(book);
        default:
            break;
        }

    }

    for_each(book.begin(), book.end(), show);

}
void deleteStr(BookMap & book)
{
    int temp_int;
    cout << "Enter phone number" << endl;
    cin >> temp_int;
    if (book.erase(temp_int) != 0)
        cout << "Operation completed successufully" << endl;
    else
        cout << "Can't find it" << endl;
}

void lookForN(BookMap &book)
{
    BookMap::iterator it;
    string temp_str;
    cout << "Enter owner of phone:" << endl;
    getline(cin, temp_str);
    for (it = book.begin(); it != book.end(); ++it)
        if (it->second == temp_str)
        {
            cout << it->first << " " << it->second << endl;
            return;
        }
    cout << "Can't find" << endl;
}

void lookForK(BookMap & book)
{
    BookMap::iterator it;
    int temp_int;
    cout << "Enter phone number, please" << endl;
    cin >> temp_int;
    it = book.find(temp_int);
    if (it != book.end())
        cout << it->first << " " << it->second << endl;
    else
        cout << "Can't find it" << endl;
}

void refreshData(BookMap & book)
{
    BookMap::iterator it;
    ofstream out;
    out.open(FILE_NAME);
    if (! out)
    {
        cerr << "Error!\n" << endl;
        exit (EXIT_FAILURE);
    }
    for (it = book.begin(); it != book.end(); it++)
        out << it->first << " " << it->second << endl;
    out.close();
}

void add(BookMap & b)
{
    string temp_str;
    int temp_int;
    cout << "Enter the number, please: " << endl;
    cin >> temp_int;
    clean();
    cout << "Enter the string, please: " << endl;
    getline(cin, temp_str);
    b.insert(PairM(temp_int, temp_str));

    cout << "Operation completed successfully" << endl;
}
void clean(void)
{
    while (cin.get() != '\n')
        ;
}
























//    for (int i = 0; i < N; i++)
//        book.insert(PairM (arr_ph[i], arr_s[i]));
//    for_each(book.begin(), book.end(), show);

//    cout << "========================================" << endl;

//    ofstream out;
//    out.open("data.txt");
//    if (! out)
//    {
//        cerr << "Error!\n" << endl;
//        return 1;
//    }
//    for (it = book.begin(); it != book.end(); it++)
//        out << it->first << " " << it->second << endl;
//    out.close();
//    book.clear();

//    cout << "=========================================" << endl;

//    ifstream in;
//    string temp_str;
//    int temp_int;
//    in.open("data.txt");
//    if (! in)
//    {
//        cerr << "Error!\n" << endl;
//        return 1;
//    }
//    while (in >> temp_str)
//    {
//        temp_int = atoi(temp_str.c_str());
//        getline(in, temp_str);
//        book.insert(PairM(temp_int, temp_str));
//    }
//    for_each(book.begin(), book.end(), show);
//    in.close();

//    return 0;
//}

