#include "VaryLong.h"
#include <cctype>
#include <cstdlib>
#include <iostream>
using namespace std;

static void clean_str(string & s)
{
    while(s.size() && s[0] == '0')
    {
        s.erase(0, 1);
    }
    if (! s.size())
        s = "0";
}
VaryLong::VaryLong(string  s)
{
    int i;
    if (s[0] == '-')
    {
        sign = false;
        s.erase(0, 1);//Delete sign from string
    }
    else
        sign = true;
    while (s.size()>1 && s[0] == '0')
        s.erase(0, 1);
    if (!s.size())
        throw "No string";
    arr = new int[s.size()];
    n = s.size();
    for (i = 0; i < s.size(); i++)
    {
        if (!isdigit(s[i]))
           throw s;
        arr[i]=s[i] - '0';
    }
}
const string VaryLong::getStr()
{
    string temp = "";
    if (sign == false)
        temp = temp + '-';
    for (int i = 0; i < n; i++)
        temp = temp + (char)(arr[i] + '0');

    return temp;
}
VaryLong::VaryLong(const VaryLong & v)
{
    n = v.n;
    sign = v.sign;
    arr = new int[n];
    for (int i = 0; i < n; i++)
        arr[i] = v.arr[i];
}

VaryLong & VaryLong::operator=(const VaryLong & v)
{
    if (this == &v)
        return *this;
    delete [] arr;
    n = v.n;
    sign = v.sign;
    arr = new int[n];
    for (int i = 0; i < n; i++)
        arr[i] = v.arr[i];

    return *this;
}
VaryLong VaryLong::operator+(VaryLong & v)
{

    string temp_str = "";
    int nextvalue=0, i;
    VaryLong max, min, temp_vl1, temp_vl2;
    if (sign && v.sign)
    {

        if ((*this) > v)
        {
            max = *this;
            min = v;
        }
        else
        {
            min = *this;
            max = v;
        }

        while (max.n && min.n)
        {
            max.n--;
            min.n--;
            nextvalue += max.arr[max.n] + min.arr[min.n];
            temp_str = (char)(nextvalue % 10 + '0') + temp_str;
            nextvalue = nextvalue / 10;
        }
        while (max.n)
        {
            max.n--;
            nextvalue += max.arr[max.n];
            temp_str = (char)(nextvalue % 10 + '0') + temp_str;
            nextvalue = nextvalue / 10;
        }
        if (nextvalue)
           temp_str = (char)(nextvalue + '0') + temp_str;
        clean_str(temp_str);
        return temp_str;

    }
    else if (sign == true && v.sign == false)
    {
        VaryLong temp_new = ABS(v);
        if ((*this) >= temp_new)
        {
            temp_vl1 = *this;
            int vn = v.n;
            while (vn--)
            {
                temp_vl1.n--;
                if (temp_vl1.arr[temp_vl1.n] >= v.arr[vn])
                    temp_str = (char)(temp_vl1.arr[temp_vl1.n] - v.arr[vn] + '0') + temp_str;
                else
                {
                    temp_vl1.arr[temp_vl1.n - 1]--;
                    temp_vl1.arr[temp_vl1.n] += 10;
                    temp_str = (char)(temp_vl1.arr[temp_vl1.n] - v.arr[vn] + '0') + temp_str;

                }
            }
            while (temp_vl1.n)
            {
                temp_vl1.n--;
                if (temp_vl1.n == 0 && temp_vl1.arr[0] == 0)
                    break;
                if (temp_vl1.arr[temp_vl1.n] < 0)
                {
                    temp_vl1.arr[temp_vl1.n - 1]--;
                    temp_vl1.arr[temp_vl1.n] += 10;
                }
                temp_str = (char)(temp_vl1.arr[temp_vl1.n] + '0') + temp_str;
            }
            clean_str(temp_str);
            return temp_str;
        }
        else
        {
            temp_vl1 = *this;
            !temp_vl1;
            temp_vl2 = v;
            !temp_vl2;
            temp_vl2 = temp_vl2 + temp_vl1;
            !temp_vl2;
            return temp_vl2;
        }
    }
    else if (sign == false && v.sign == true)
    {
        return v + (*this);
    }
    else
    {
        temp_vl1 = *this;
        temp_vl2 = v;
        !temp_vl1;
        !temp_vl2;
        temp_vl1 = temp_vl1 + temp_vl2;
        !temp_vl1;
        return temp_vl1;
    }
}
VaryLong operator!(VaryLong & v)
{
    if (v.getStr() != "0")
        v.sign = !v.sign;
    return v;
}


