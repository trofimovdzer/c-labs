#ifndef VARYLONG_H_
#define VARYLONG_H_
#include <string>
#include <QString>
using std::string;
class VaryLong
{
    int * arr;
    int n;
    bool sign;
public:
    //Basic methods
    VaryLong() { arr = NULL; n = 0; }
    VaryLong(string); //exeption: string , const char *
    VaryLong(QString s) { arr = NULL; n = 0; *this = VaryLong(s.toStdString()); }
    const string getStr();
    QString getQStr() { return QString::fromStdString(getStr()); }
    ~VaryLong(){ delete [] arr; arr = NULL; }
    VaryLong(const VaryLong &);
    VaryLong & operator=(const VaryLong &);
    //End basic methods
    //operators + - < > <= >= == != * / % ^
    VaryLong operator+(VaryLong &); //Basic operator. All ather opertors works with
                                    //help operator+.
    VaryLong operator-(VaryLong &);
    bool operator>(VaryLong &);
    bool operator==(VaryLong &);
    bool operator!=(VaryLong & v) { return (!((*this) == v)); }
    bool operator<(VaryLong & v) { return !(((*this) > v) || ((*this) == v)); }
    bool operator<=(VaryLong & v) { return (((*this) < v) || ((*this) == v)); }
    bool operator>=(VaryLong & v) { return (((*this) > v) || ((*this) == v)); }
    VaryLong operator*(VaryLong & v);
    VaryLong operator/(VaryLong & v);
    VaryLong operator%(VaryLong &v); //exeption: const char *
    VaryLong operator^(VaryLong & v);
    //Friends
    friend VaryLong operator!(VaryLong & v);
    friend VaryLong ABS(VaryLong v) { return (v.sign ? v : (!v)); }
};

#endif // HFILE_H
