#include <iostream>
#include <list>
#include <string>
#include <showmenu.h>
#include <algorithm>
using namespace std;

struct Item
{
    string name;
    int nice;
};
typedef list<Item> PQueue;

const int N =5;
const string str[N] = { "Add" , "Delete" , "Show",
        "Length", "Clear" };

//funcltions

bool operator<(const Item & i1, const Item & i2)
{
    if(i1.nice < i2.nice)
        return true;
    else
        return false;
}

void add(PQueue & pq)
{
    Item temp;
    cout << "Enter name of process: " << endl;
    cin >> temp.name;
    cout << "Enter nice: " << endl;
    cin >> temp.nice;
    pq.push_back(temp);
    cout << "Operation completed successfully" << endl;
    pq.sort();
}

void del(PQueue & pq)
{
    if (pq.size() == 0)
    {
        cout << "Queue is empty" << endl;
        return;
    }
    Item temp;
    list<Item>::reverse_iterator pend = pq.rbegin();
    cout << "Delete:" << endl;
    cout << (*pend).name << " " << (*pend).nice << endl;
    pq.pop_back();
}

void show(Item & it)
{
    cout << it.name << "  " << it.nice << endl;
}

int main()
{
    PQueue pq;
    Menu menu(str, N);
    char ch;
    while ((ch = menu.show()) != 'q')
        switch (ch)
        {
        case 'a':
            add(pq);
            break;
        case 'b':
            del(pq);
            break;
        case 'c':
            for_each(pq.begin(), pq.end(), show);
            break;
        case 'd':
            cout << "Size: " << pq.size() << endl;
            break;
        case 'e':
            cout << "Operation completed successfully." << endl;
            pq.clear();
            break;

        default:
            break;
        }


    return 0;
}

