#include <iostream>
#include "matrix.h"
#include <ctime>
#include <cstdlib>
#include <showmenu.h>

using namespace std;

const string strs[] =
{
    "Sum of matrices.",
    "Subtraction of matrices.",
    "Multiplication of matrices.",
    "The matrix is multiplied by the number.",
    "Determinant of matrix",
    "Reverse matrix",
    "Transposed matrix"
};
void twoMatrices(char ch);
void oneMatrix(char ch);
int main()
{
    Menu menu(strs, 7);
    char ch;
    while ((ch = menu.show()) != 'q')
    {
        switch (ch) {
        case 'a':
            twoMatrices('+');
            break;
        case 'b':
            twoMatrices('-');
            break;
         case 'c':
            twoMatrices('*');
            break;
        case 'd':
            oneMatrix('*');
            break;
        case 'e':
            oneMatrix('d');
            break;
        case 'f':
            oneMatrix('r');
            break;
        case 'g':
            oneMatrix('g');
            break;
        default:
            break;
        }
    }

    return 0;
}
void oneMatrix(char ch)
{
    int n, m;

    cout << "Enter N:" << endl;
    cin >> n;
    cout << "Enter M:" << endl;
    cin >> m;
    Matrix A(n, m);
    cout << "Enter matrix:" << endl;
    A.fill();


    Matrix E;

    switch (ch) {
    case '*':
        int x;
        cout << "Enter number:" << endl;
        cin >> x;
        E = A * x;
        break;
    case 'd':
        cout << "Determinant: " <<  det(A) << endl;
        return ;
        break;
    case 'r':
        E = A.getRevers();
        break;
    case 'g':
        E=A.trans();
        break;
    default:
        break;
    }

    cout << "Matrix: " << endl
            << A << endl
               << "Result: " << endl
                  << E << endl;
}

void twoMatrices(char ch)
{
    int n, m;

    cout << "First matrix." << endl;
    cout << "Enter N:" << endl;
    cin >> n;
    cout << "Enter M:" << endl;
    cin >> m;
    Matrix A(n, m);
    cout << "Enter first matrix:" << endl;
    A.fill();

    cout << "Second matrix." << endl;
    cout << "Enter N:" << endl;
    cin >> n;
    cout << "Enter M:" << endl;
    cin >> m;
    Matrix B(n, m);
    cout << "Enter second matrix:" << endl;
    B.fill();

    Matrix E;

    switch (ch) {
    case '+':
        E = A + B;
        break;
    case '-':
        E = A - B;
        break;
    case '*':
        E = A * B;
        break;
    default:
        break;
    }

    cout << "1." << endl
            << A << endl
               << "2." << endl
                  << B << endl
                     << "==" << endl
                        << E << endl;
}

