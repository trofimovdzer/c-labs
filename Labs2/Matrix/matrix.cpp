#include "matrix.h"
#include <iostream>
#include <cstdlib>

using namespace std;

Matrix::Matrix(int n, int m) : mat(n)
{
    N = n;
    M = m;
    for(int j = 0; j < N; j++)
        mat[j].resize(m);
}

void Matrix::fill()
{
    int i, j;
    for (i = 0; i < N; i++)
        for (j = 0; j < M; j++)
            if (! (cin >> mat[i][j]))
                throw "Bad input";
}
void Matrix::show()
{
    int i, j;
    for (i = 0; i < N; i++)
    {
        cout << "|  ";
        for (j = 0; j < M; j++)
        {
            cout.width(4);
            cout << mat[i][j] << ((j != (M - 1)) ? ",  ": "");
        }
        cout << " |" << endl;
    }
}
void Matrix::random_fill(void)
{
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            mat[i][j] = rand() % 100;

}

bool Matrix::operator==(Matrix & m)
{
    if (N != m.N || M != m.M)
        return false;
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            if (mat[i][j] != m.mat[i][j])
                return false;

    return true;
}

Matrix Matrix::operator+(Matrix & m)
{
    if (m.N != N || m.M != M)
        throw "Size of two matrix should be equel";
    Matrix temp(N, M);
    for (int i = 0; i < N; i++)
        for(int j = 0; j < M; j++)
            temp[i][j] = mat[i][j] + m.mat[i][j];

    return temp;
}
Matrix Matrix::operator-(Matrix & m)
{
    if (m.N != N || m.M != M)
        throw "Size of two matrix should be equel";
    Matrix temp(N, M);
    for (int i = 0; i < N; i++)
        for(int j = 0; j < M; j++)
            temp[i][j] = mat[i][j] - m.mat[i][j];

    return temp;
}
Matrix Matrix::operator *(int n)
{
    Matrix temp(N, M);
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            temp.mat[i][j] = mat[i][j] * n;

    return temp;
}
Matrix Matrix::operator*(Matrix & m)
{
    if (M != m.N)
        throw "Bad size of matrix in multiplication";
    Matrix temp(N, m.M);
    double t =0;
    for(int i = 0; i < temp.N; i++)
        for(int j = 0; j < temp.M; j++)
        {
            t = 0;
            for (int x = 0; x < M; x++)
                t += mat[i][x] * m.mat[x][j];
            temp.mat[i][j] = t;
        }

    return temp;
}


double det(Matrix & m)
{
    if (m.M != m.N)
        throw "Can't find det";
    if (m.M == 1)
        return m.mat[0][0];
    if (m.M == 2)
        return m.mat[0][0] * m.mat[1][1] - m.mat[0][1] * m.mat[1][0];
    double d = 0;
    Matrix temp (m.M-1, m.M-1);
    for (int x = 0; x < m.M; x++)
    {
        temp = setTemp(m, x);
        if (x % 2 == 0)
            d += m.mat[0][x] * det(temp);
        else
            d -= m.mat[0][x] * det(temp);
    }

    return d;
}
Matrix setTemp(Matrix & m, int x)
{
    Matrix temp(m.M-1, m.M-1);
    for (int i = 1; i < m.M; i++)
        for(int j = 0; j < m.M; j++)
        {
            if (j == x)
                continue;
            if (j < x)
                temp[i -1][j] = m.mat[i][j];
            else
                temp[i -1][j-1] = m.mat[i][j];
        }

    return temp;
}

Matrix Matrix::getRevers(void) //exeption = const char *
{
    if (M != N || det(*this) == 0)
        throw "Can't find revers matrix";
    Matrix temp(M - 1, M - 1);
    Matrix result(M,M);
    for (int i = 0; i < M; i++)
        for (int j = 0; j < M; j++)
        {
            temp = matForRevers(*this, i, j);
            if ((i + j) % 2 == 0)
                result[i][j] = det(temp) / det(*this);
            else
                result[i][j] = - det(temp) / det(*this);
        }

    return result;
}

Matrix matForRevers(Matrix & m, int x, int y)
{
    Matrix temp(m.M -1, m.M-1);
    int h =0, w;
    for (int i = 0; i < m.M; i++)
    {
        if( i == x)
            continue;
        w = 0;
        for (int j = 0; j < m.M; j++)
        {
            if ( j == y)
                continue;
            temp.mat[h][w] = m.mat[i][j];
            w++;
        }
        h++;
    }

     return temp;
}

Matrix Matrix::trans(void)
{
    Matrix temp(M, N);
    for(int i = 0; i < N; i++)
        for(int j = 0; j < M; j++)
            temp[j][i] = mat[i][j];

    return temp;
}
